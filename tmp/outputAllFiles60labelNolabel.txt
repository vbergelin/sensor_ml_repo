---- INPUT VECTOR ----
sequence length (sec): 30.0
no lable vs lable: 0.6
training vs testing data: 0.85
sub seq length (sec): 5.0
---- LABEL PRIOR ----
{0: [600, 7200], 1: [300, 600]}
---- TRAIN TEST -----
Train: /Users/victorbergelin/Google Drive/GD Data/Data/Rawdataimport/subjects/1*/ph2/
Test: 
---- MAIN RUN ----
*** Editor comment *** : Running test with all data 
len(data) = 10
len sequences = 21542
len(X) = 21542
0.6is no_lable_vs_lable value
2151 is new 0 class length. New total length: 3585 1 class length is: 1434
Results:
             precision    recall  f1-score   support

          0      0.659     0.836     0.737      1938
          1      0.586     0.349     0.437      1290

avg / total      0.630     0.641     0.617      3228

Run time: 561.056545973
0.64126394052
Top positive:
1.080360 1        freq_var0
1.080360 1        var0
1.018816 0        mean3
0.478771 0        mean0
0.249430 1        mean4
0.208744 1        mean2
0.198488 0        diff_feat2
0.168298 1        mean1
0.165512 0        fmean3
0.151816 1        fmean1
0.151816 1        fmean5
0.056644 1        fmean2
0.056644 1        fmean4
0.045121 1        diff_freq0
0.036512 0        freq_var4
0.036512 0        var4
0.035883 1        diff_feat0
0.028536 0        diff_feat5
0.024210 0        diff_feat1
0.016471 1        diff_freq4
0.016471 1        diff_freq2
0.015616 0        diff_freq3
0.008098 1        freq_var5
0.008098 1        var5
0.007012 0        diff_freq5
0.007012 0        diff_freq1
0.004112 0        fmean0
0.003407 1        diff_feat4
0.001448 1        diff_feat3
0.000740 0        var2

Top negative:
-0.000088 0        freq_var3
-0.000088 0        var3
-0.000415 0        freq_var1
-0.000415 0        var1
-0.000740 1        var2
-0.000740 1        freq_var2
-0.001448 0        diff_feat3
-0.003407 0        diff_feat4
-0.004112 1        fmean0
-0.007012 1        diff_freq5
-0.007012 1        diff_freq1
-0.008098 0        freq_var5
-0.008098 0        var5
-0.015616 1        diff_freq3
-0.016471 0        diff_freq2
-0.016471 0        diff_freq4
-0.024210 1        diff_feat1
-0.028536 1        diff_feat5
-0.035883 0        diff_feat0
-0.036512 1        freq_var4
-0.036512 1        var4
-0.045121 0        diff_freq0
-0.056644 0        fmean2
-0.056644 0        fmean4
-0.151816 0        fmean1
-0.151816 0        fmean5
-0.165512 1        fmean3
-0.198488 1        diff_feat2
-1.080360 0        freq_var0
-1.080360 0        var0
