---- INPUT VECTOR ----
sequence length (sec): 30.0
no lable vs lable: 0.85
training vs testing data: 0.85
sub seq length (sec): 5.0
---- LABEL PRIOR ----
{0: [600, 7200], 1: [300, 600]}
---- TRAIN TEST -----
Train: /Users/victorbergelin/Google Drive/GD Data/Data/Rawdataimport/subjects/100/ph2/
Test: 
---- MAIN RUN ----
*** Editor comment *** : Running test with all data 
len(data) = 1
len sequences = 1385
len(X) = 1385
Results:
             precision    recall  f1-score   support

          1      1.000     1.000     1.000       366

avg / total      1.000     1.000     1.000       366

Run time: 31.7162339687
1.0
Top positive:

Top negative:
